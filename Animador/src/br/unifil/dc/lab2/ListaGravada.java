package br.unifil.dc.lab2;

import javax.swing.JPanel;
import java.awt.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Random;

/**
 * Write a description of class ListaGravada here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ListaGravada implements Transparencia
{
    /**
     * Constructor for objects of class ListaGravada
     */
    public ListaGravada(List<Integer> lista, List<Color> coresIndices, String nome) {
        this.lista = lista;
        this.nome = nome;
        this.coresIndices = coresIndices;
    }

    public void pintar(Graphics2D pincel, JPanel contexto) {
        Dimension dim = contexto.getSize();

        Color ligthtcyiano = new Color(25,255,255);
        Color burntOrange = new Color(179,116,0);
        float thickness = 3f;
        Stroke stroke = pincel.getStroke();

        pincel.setColor(Color.BLACK);
        pincel.setStroke(new BasicStroke(5f));
        //pincel.drawRect(100, 25, 550,550);

        int espaco = 30;
        Integer elementos[] = new Integer[lista.size()];
        for (int i = 0; i < lista.size(); i++) {
            elementos[i] = lista.get(i);
        }
        pesquisarMaior(elementos);

        List <Integer> copia = new ArrayList<>(lista);
        List <Integer> salvaLista = new ArrayList<Integer>(listaValores(copia));
        int salvaIndice = 0;
        int tamanhoLista = lista.size();
        double porcentagem = (100 / lista.size()); // o valor da porcentagem, deve diminuir com 100%
        double diminuir = 100 - porcentagem;
        double resultado = 0;
        for (int i = 0; i < lista.size(); i++) {
            for (int j = 0; j < salvaLista.size(); j++) {
                if (lista.get(i).equals(salvaLista.get(j))) {
                    salvaIndice = j;
                }
            }
//            for (int k = 0; k < tamanhoLista; k++) {
//                if (salvaIndice == 0) {
//                    desenharColunas(pincel, ligthtcyiano, thickness, espaco, 300, Color.BLACK);
//                } else if (salvaIndice == k) {
//                    desenharColunas(pincel, ligthtcyiano, thickness, espaco, (int) (300 * (diminuir / 100)), Color.BLACK);
//                }
//                resultado = diminuir - porcentagem;
//            }
          if (salvaIndice == 0) {
              desenharColunas(pincel, ligthtcyiano, thickness, espaco, 300, Color.BLACK);
           }
           if (salvaIndice == 1) {
               desenharColunas(pincel, ligthtcyiano,thickness, espaco, (int) (300 * 0.8334), Color.BLACK);
           }
           if (salvaIndice == 2) {
               desenharColunas(pincel, ligthtcyiano, thickness, espaco, (int) (300 * 0.6668), Color.BLACK);
           }
           if (salvaIndice == 3) {
               desenharColunas(pincel, ligthtcyiano, thickness, espaco, (int) (300 * 0.5002), Color.BLACK);
           }
          if (salvaIndice == 4) {
               desenharColunas(pincel, ligthtcyiano, thickness, espaco, (int) (300 * 0.3336), Color.BLACK);
           }
          if (salvaIndice == 5) {
              desenharColunas(pincel, ligthtcyiano, thickness, espaco, (int) (300 * 0.167), Color.BLACK);
          }
            espaco += 122;
        }
        espaco -= 20;
        pincel.drawRect(10,150, espaco,400); // Se ajusta com o tamanho de elementos
        // for (int i = 0; i < lista.size(); i++) {
           // pincel.setColor(Color.BLUE);
           // pincel.drawRect(proximo,200,30, -lista.get(i));
           // proximo += 40;
        // }
        //throw new RuntimeException("Funcionalidade ainda não implementada pelo aluno");
    }

    private void desenharColunas(Graphics2D pincel, Color ligthtcyiano,float thickness, int espaco, int i2, Color black) {
        pincel.setColor(ligthtcyiano);
        pincel.fillRect(espaco, 500, 72, -i2);
        pincel.setColor(black);
        pincel.setStroke(new BasicStroke(thickness));
        pincel.drawRect(espaco, 500, 72, -i2);
    }

    private static Integer pesquisarMaior(Integer[] elementos) {

        int maior = elementos[0];
        for (int i = 1; i < elementos.length; i++) {
            if(elementos[i] > maior) maior = elementos[i];
        }
        return maior;
    }

    private static List<Integer> listaValores(List <Integer> valores) {

        boolean permutando;
        do {
            permutando = false;
            for (int i = 1; i < valores.size(); i++) {
                if (valores.get(i-1) < valores.get(i)) {
                    int salvaValor = valores.get(i);
                    valores.set(i, valores.get(i-1));
                    valores.set(i-1, salvaValor);
                    permutando = true;
                }
            }
        } while (permutando);

    return valores;
}
    private List<Integer> lista;
    private List<Color> coresIndices;
    private String nome;
}
